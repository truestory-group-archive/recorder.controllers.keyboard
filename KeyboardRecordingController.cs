﻿using Gma.System.MouseKeyHook;
using Recorder.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Recorder.Controllers.Keyboard
{
    public class KeyboardRecordingController : IRecordingController
    {
        private IRecordingService _recordingService;
        private ILogger _logger;

        private char _startKey;
        private char _stopKey;

        private IKeyboardMouseEvents m_GlobalHook;

        public KeyboardRecordingController(IKeyboardConfiguration configuration, IRecordingService recordingService, ILogger logger)
        {
            _recordingService = recordingService;
            _logger = logger;
            _startKey = configuration.StartKey;
            _stopKey = configuration.StopKey;

            _logger.Log("Initialized keyboard hook");

            m_GlobalHook = Hook.GlobalEvents();
            
            m_GlobalHook.KeyPress += GlobalHookKeyPress;
        }

        private void GlobalHookKeyPress(object sender, KeyPressEventArgs e)
        {
            _logger.Log(string.Format("KeyPress: \t{0}", e.KeyChar));
            Console.WriteLine("Test");
        }
        

        ~KeyboardRecordingController()
        {
            m_GlobalHook.KeyPress -= GlobalHookKeyPress;

            //It is recommened to dispose it
            m_GlobalHook.Dispose();
        }
    }
}
