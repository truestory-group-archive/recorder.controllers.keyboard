﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recorder.Controllers.Keyboard
{
    public interface IKeyboardConfiguration
    {
        char StartKey { get; set; }
        char StopKey { get; set; }
    }
}
