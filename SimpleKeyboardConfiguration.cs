﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recorder.Controllers.Keyboard
{
    public class SimpleKeyboardConfiguration : IKeyboardConfiguration
    {
        public char StartKey { get; set; }
        public char StopKey { get; set; }

        public SimpleKeyboardConfiguration(char startKey, char stopKey)
        {
            StartKey = startKey;
            StopKey = stopKey;
        }
    }
}
